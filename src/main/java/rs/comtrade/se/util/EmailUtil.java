/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import rs.comtrade.se.exceptions.BadFormatException;
import rs.comtrade.se.model.Email;

/**
 *
 * @author vsvicevic
 */
public class EmailUtil {

    public static void validateEmail(Email email) throws BadFormatException {
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        String emailAddress = email.getEmailAddress();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(emailAddress);

        if (!matcher.matches()) {
            if (!emailAddress.contains("@")) {
                throw new BadFormatException("Email address doesn't contain '@' character");
            }
            if (!emailAddress.contains(".")) {
                throw new BadFormatException("Email address doesn't contain '.' character");
            }
            if (isLastCharacter(emailAddress, '@') || isLastCharacter(emailAddress, '.')) {
                throw new BadFormatException("'@' or '.' can't be last character");
            }
            if (isFirstCharacter(emailAddress, '@') || isFirstCharacter(emailAddress, '.')) {
                throw new BadFormatException("'@' or '.' can't be first character");
            }
            if (isMonkeyNextToDot(emailAddress)) {
                throw new BadFormatException("'@' can't be next to '.'");
            }
            throw new BadFormatException("Bad format!");
        }
    }

    private static boolean isMonkeyNextToDot(String emailAddress) {
        for (int i = 0; i < emailAddress.length() - 2; i++) {
            if (emailAddress.charAt(i) == '@' && emailAddress.charAt(i + 1) == '.') {
                return true;
            }
        }
        return false;
    }

    private static boolean isLastCharacter(String emailAddress, char character) {
        return emailAddress.indexOf(character) == emailAddress.length() - 1;
    }

    private static boolean isFirstCharacter(String emailAddress, char character) {
        return emailAddress.indexOf(character) == 0;
    }
}
