/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.services;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.comtrade.se.model.Effort;
import rs.comtrade.se.model.Permission;
import rs.comtrade.se.model.Role;
import rs.comtrade.se.model.Subtask;
import rs.comtrade.se.model.Task;
import rs.comtrade.se.model.User;
import rs.comtrade.se.repositories.EffortRepository;
import rs.comtrade.se.repositories.PermissionRepository;
import rs.comtrade.se.repositories.RoleRepository;
import rs.comtrade.se.repositories.SubtaskRepository;
import rs.comtrade.se.repositories.TaskRepository;
import rs.comtrade.se.repositories.UserRepository;

/**
 *
 * @author vsvicevic
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private SubtaskRepository subtaskRepository;
    @Autowired
    private EffortRepository effortRepository;

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll()
                .forEach(user -> users.add(user));
        return users;
    }

    public User getUser(int user_id) {
        return (User) userRepository.findOne(user_id);
    }

    public User insertUser(User user) {
        return (User) userRepository.save(user);
    }

    public User updateUser(User user, int user_id) {
        user.setUser_id(user_id);
        return (User) userRepository.save(user);
    }

    public void deleteUser(int user_id) {
        userRepository.delete(user_id);
    }

    public User assignRole(int user_id, int role_id) {
        User user = userRepository.findOne(user_id);
        Role role = roleRepository.findOne(role_id);
        List<Role> roles = user.getRoles();
        roles.add(role);
        return (User) userRepository.save(user);
    }

    public User assignTask(int user_id, int task_id) {
        User user = userRepository.findOne(user_id);
        Task task = taskRepository.findOne(task_id);
        task.setUser(user);
        taskRepository.save(task);
        return user;
    }

    public User assignPermission(int user_id, int role_id, int permission_id) {
        User user = userRepository.findOne(user_id);
        Permission permission = permissionRepository.findOne(permission_id);
        Role role = user.findRoleById(role_id);
        role.getPermissions().add(permission);
        return userRepository.save(user);
    }

    public Response assignSubtask(int user_id, int task_id, int subtask_id) {
        User user = userRepository.findOne(user_id);
        Subtask subtask = subtaskRepository.findOne(subtask_id);
        Task task = user.getTasks().stream().filter(t -> t.getTask_id() == task_id).findFirst().get();
        if (task == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("User didn't assigned to that task!")
                    .build();
        }
        subtask.setTask(task);
        subtaskRepository.save(subtask);
        return Response.status(Response.Status.OK)
                .entity("Succesfully assigned subtask to user!")
                .build();
    }

    public Response assignEffortToTask(int user_id, int task_id, Effort effort) {
        User user = userRepository.findOne(user_id);
        try {
            Task task = user.getTasks().stream().filter(t -> t.getTask_id() == task_id).findFirst().get();
            effort.setUser(user);
            effort.setTask(task);
            effortRepository.save(effort);
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("User didn't assigned to that task!")
                    .build();
        }
        return Response.status(Response.Status.OK)
                .entity("Succesfully assigned effort to user's task!")
                .build();
    }

    public Response assignEffortToSubtask(int user_id, int subtask_id, Effort effort) {
        User user = userRepository.findOne(user_id);
        Subtask subtask = user.findSubtaskById(subtask_id);
        if (subtask == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("User didn't assigned to that subtask!")
                    .build();
        }
        effort.setUser(user);
        effort.setSubtask(subtask);
        effortRepository.save(effort);
        return Response.status(Response.Status.OK)
                .entity("Succesfully assigned effort to user's subtask!")
                .build();
    }
}
