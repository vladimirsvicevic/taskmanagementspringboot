/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.comtrade.se.model.Role;
import rs.comtrade.se.repositories.RoleRepository;

/**
 *
 * @author vsvicevic
 */
@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role getRole(int role_id) {
        return (Role) roleRepository.findOne(role_id);
    }

    public Role insertRole(Role role) {
        return (Role) roleRepository.save(role);
    }

    public Role updateRole(Role role, int role_id) {
        role.setRole_id(role_id);
        return (Role) roleRepository.save(role);
    }

    public void deleteRole(int role_id) {
        roleRepository.delete(role_id);
    }

    public List<Role> getRoles() {
        List<Role> roles = new ArrayList<>();
        roleRepository.findAll()
                .forEach(role -> roles.add(role));
        return roles;
    }

}
