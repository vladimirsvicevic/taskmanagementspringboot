/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.services;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.comtrade.se.model.Task;
import rs.comtrade.se.model.User;
import rs.comtrade.se.repositories.TaskRepository;

/**
 *
 * @author vsvicevic
 */
@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public List<Task> getTasks() {
        List<Task> tasks = new ArrayList<>();
        taskRepository.findAll()
                .forEach(task -> tasks.add(task));
        return tasks;
    }

    public Task getTask(int task_id) {
        return (Task) taskRepository.findOne(task_id);
    }

    public Task insertTask(Task task) {
        return (Task) taskRepository.save(task);
    }

    public Task updateTask(Task task, int task_id) {
        task.setTask_id(task_id);
        return taskRepository.save(task);
    }

    public void deleteTask(int task_id) {
        taskRepository.delete(task_id);
    }

    public void assignUser(int task_id, int user_id) {
        Task task = taskRepository.findOne(task_id);
        User user = new User();
        user.setUser_id(user_id);
        task.setUser(user);
        taskRepository.save(task);
    }

}
