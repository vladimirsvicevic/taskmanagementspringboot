/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.comtrade.se.model.Country;
import rs.comtrade.se.repositories.CountryRepository;

/**
 *
 * @author vsvicevic
 */
@Service
public class CountryService {
    
    @Autowired
    private CountryRepository countryRepository;
    
    public List<Country> getCountries() {
        List<Country> countries = new ArrayList<>();
        countryRepository.findAll()
                .forEach(country -> countries.add(country));
        return countries;
        
    }

    public Country getCountry(int country_id) {
        return (Country) countryRepository.findOne(country_id);
    }

    public Country insertCountry(Country country) {
        return (Country) countryRepository.save(country);
    }

    public Country updateCountry(Country country, int country_id) {
        country.setCountry_id(country_id);
        return (Country) countryRepository.save(country);
    }

    public void deleteCountry(int country_id) {
        countryRepository.delete(country_id);
    }
}
