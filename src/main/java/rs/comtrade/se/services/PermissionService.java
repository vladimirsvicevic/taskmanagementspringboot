/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.comtrade.se.model.Permission;
import rs.comtrade.se.repositories.PermissionRepository;

/**
 *
 * @author vsvicevic
 */
@Service
public class PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    public List<Permission> getPermissions() {
        List<Permission> permissions = new ArrayList<>();
        permissionRepository.findAll()
                .forEach(permission -> permissions.add(permission));
        return permissions;
    }

    public Permission getPermission(int permission_id) {
        return (Permission) permissionRepository.findOne(permission_id);
    }

    public Permission insertPermission(Permission permission) {
        return (Permission) permissionRepository.save(permission);
    }

    public Permission updatePermission(Permission permission, int permission_id) {
        permission.setPermission_id(permission_id);
        return (Permission) permissionRepository.save(permission);
    }

    public void deletePermission(int permission_id) {
        permissionRepository.delete(permission_id);
    }
}
