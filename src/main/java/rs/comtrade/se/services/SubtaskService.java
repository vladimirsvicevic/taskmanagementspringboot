/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.comtrade.se.model.Subtask;
import rs.comtrade.se.repositories.SubtaskRepository;

/**
 *
 * @author vsvicevic
 */
@Service
public class SubtaskService {

    @Autowired
    private SubtaskRepository subtaskRepository;

    public List<Subtask> getSubtasks() {
        List<Subtask> subtasks = new ArrayList<>();
        subtaskRepository.findAll()
                .forEach(subtask -> subtasks.add(subtask));
        return subtasks;
    }

    public Subtask getSubtask(int subtask_id) {
        return subtaskRepository.findOne(subtask_id);
    }

    public Subtask insertSubtask(Subtask subtask) {
        return subtaskRepository.save(subtask);
    }

    public Subtask updateSubtask(Subtask subtask, int subtask_id) {
        subtask.setSubtask_id(subtask_id);
        return subtaskRepository.save(subtask);
    }

    public void deleteSubtask(int subtask_id) {
        subtaskRepository.delete(subtask_id);
    }
}
