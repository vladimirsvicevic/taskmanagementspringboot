/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import javax.ws.rs.core.Response;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author vsvicevic
 */
public interface Emails {

    @RequestMapping(method = RequestMethod.POST, value = "/email/validator")
    Response validateEmailAddress(@RequestBody Email email);
    
}
