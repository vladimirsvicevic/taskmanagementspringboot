/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author vsvicevic
 */
public interface Roles {

    @RequestMapping(method = RequestMethod.DELETE, value = "/{role_id}")
    void deleteRole(@PathVariable int role_id);

    @RequestMapping(method = RequestMethod.GET, value = "")
    List<Role> getRoles();

    @RequestMapping(method = RequestMethod.POST, value = "")
    Role insertRole(@RequestBody Role role);

    @RequestMapping(method = RequestMethod.PUT, value = "/{role_id}")
    Role updateRole(@RequestBody Role role, @PathVariable int role_id);
    
}
