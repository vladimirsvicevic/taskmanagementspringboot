/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import javax.ws.rs.core.Response;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author vsvicevic
 */
public interface Users {

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/subtask/{subtask_id}/effort")
    Response assignEffortToSubtask(@PathVariable int user_id, @PathVariable int subtask_id, @RequestBody Effort effort);

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/task/{task_id}/effort")
    Response assignEffortToTask(@PathVariable int user_id, @PathVariable int task_id, @RequestBody Effort effort);

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/roles/{role_id}/permission/{permission_id}")
    User assignPermission(@PathVariable int user_id, @PathVariable int role_id, @PathVariable int permission_id);

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/role/{role_id}")
    User assignRole(@PathVariable int user_id, @PathVariable int role_id);

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/task/{task_id}/subtask/{subtask_id}")
    Response assignSubtask(@PathVariable int user_id, @PathVariable int task_id, @PathVariable int subtask_id);

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/task/{task_id}")
    User assignTask(@PathVariable int user_id, @PathVariable int task_id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/{user_id}")
    void deleteUser(@PathVariable int user_id);

    @RequestMapping(method = RequestMethod.GET, value = "")
    MappingJacksonValue getUsers(@RequestParam(value = "include", defaultValue = "notInclude") String include);

    @RequestMapping(method = RequestMethod.POST, value = "")
    User insertUser(@RequestBody User user);

    @RequestMapping(method = RequestMethod.PUT, value = "/{user_id}")
    User updateUser(@RequestBody User user, @PathVariable int user_id);
    
}
