/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author vsvicevic
 */
public interface Permissions {

    @RequestMapping(method = RequestMethod.DELETE, value = "/{permission_id}")
    void deletePermission(@PathVariable int permission_id);

    @RequestMapping(method = RequestMethod.GET, value = "")
    List<Permission> getPermissions();

    @RequestMapping(method = RequestMethod.POST, value = "")
    Permission insertPermission(@RequestBody Permission permission);

    @RequestMapping(method = RequestMethod.PUT, value = "/{permission_id}")
    Permission updatePermission(@RequestBody Permission permission, @PathVariable int permission_id);
    
}
