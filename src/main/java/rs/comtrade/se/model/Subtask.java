/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author vsvicevic
 */
@Entity(name = "Subtask")
@Table(name = "subtask")
public class Subtask implements Serializable {

    private int subtask_id;
    private String title;
    private String description;
    private String status;
    private Date date;
    private Task task;
    private List<Effort> efforts;

    public Subtask() {
    }

    public Subtask(int subtask_id, String title, String description, String status, Date date, Task task) {
        this.subtask_id = subtask_id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.date = date;
        this.task = task;
    }

    @ManyToOne
    @JoinColumn(name = "task_id")
    @JsonIgnore
    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "subtask_id")
    @JsonView(View.SubtaskIncludeView.class)
    public int getSubtask_id() {
        return subtask_id;
    }

    public void setSubtask_id(int subtask_id) {
        this.subtask_id = subtask_id;
    }

    @Column(name = "title")
    @JsonView(View.SubtaskIncludeView.class)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "description")
    @JsonView(View.SubtaskIncludeView.class)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "status")
    @JsonView(View.SubtaskIncludeView.class)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @JsonView(View.SubtaskIncludeView.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @OneToMany(mappedBy = "subtask")
    @JsonView(View.SubtaskEffortView.class)
    public List<Effort> getEfforts() {
        return efforts;
    }

    public void setEfforts(List<Effort> efforts) {
        this.efforts = efforts;
    }

}
