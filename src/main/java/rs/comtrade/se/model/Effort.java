/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author vsvicevic
 */
@Entity(name = "Effort")
@Table(name = "effort")
public class Effort implements Serializable {

    private int effort_id;
    private User user;
    private Task task;
    private Subtask subtask;
    private int hours;
    private Date date;

    public Effort() {
    }

    public Effort(int effort_id, User user, Task task, Subtask subtask, int hours, Date date) {
        this.effort_id = effort_id;
        this.user = user;
        this.task = task;
        this.subtask = subtask;
        this.hours = hours;
        this.date = date;
    }

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @JsonView(View.BaseEffortView.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "effort_id")
    @JsonView(View.BaseEffortView.class)
    public int getEffort_id() {
        return effort_id;
    }

    public void setEffort_id(int effort_id) {
        this.effort_id = effort_id;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "task_id", nullable = true)
    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @ManyToOne
    @JoinColumn(name = "subtask_id", nullable = true)
    @JsonIgnore
    public Subtask getSubtask() {
        return subtask;
    }

    public void setSubtask(Subtask subtask) {
        this.subtask = subtask;
    }

    @Column(name = "hours")
    @JsonView(View.BaseEffortView.class)
    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

}
