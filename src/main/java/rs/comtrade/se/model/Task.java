/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author vsvicevic
 */
@Entity(name = "Task")
@Table(name = "task")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "task_id")
public class Task implements Serializable {

    private int task_id;
    private String title;
    private String description;
    private String status;
    private Date date;
    private User user;
    private List<Subtask> subtasks;
    private List<Effort> efforts;

    public Task() {
    }

    public Task(int task_id, String title, String description, String status, Date date, User user) {
        this.task_id = task_id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.date = date;
        this.user = user;
    }

    public Task(int task_id, String title, String description, String status, Date date, User user, List<Subtask> subtasks) {
        this.task_id = task_id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.date = date;
        this.user = user;
        this.subtasks = subtasks;
    }

    @Column(name = "description")
    @JsonView(View.TaskIncludeView.class)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "task_id")
    @JsonView(View.TaskIncludeView.class)
    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    @Column(name = "title")
    @JsonView(View.TaskIncludeView.class)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "status")
    @JsonView(View.TaskIncludeView.class)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @JsonView(View.TaskIncludeView.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonProperty
    @JsonBackReference
    public User getUser() {
        return user;
    }

    @JsonProperty
    public void setUser(User user) {
        this.user = user;
    }

    @OneToMany(mappedBy = "task")
    @JsonView(View.SubtaskIncludeView.class)
    public List<Subtask> getSubtasks() {
        return subtasks;
    }

    public void setSubtasks(List<Subtask> subtasks) {
        this.subtasks = subtasks;
    }

    @OneToMany(mappedBy = "task")
    @JsonView(View.TaskEffortView.class)
    public List<Effort> getEfforts() {
        return efforts;
    }

    public void setEfforts(List<Effort> efforts) {
        this.efforts = efforts;
    }

}
