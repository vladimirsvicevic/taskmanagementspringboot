/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author vsvicevic
 */
public interface Subtasks {

    @RequestMapping(method = RequestMethod.DELETE, value = "/{subtask_id}")
    void deleteSubtask(@PathVariable int subtask_id);

    @RequestMapping(method = RequestMethod.GET, value = "/{subtask_id}")
    Subtask getSubtask(@PathVariable int subtask_id);

    @RequestMapping(method = RequestMethod.GET, value = "")
    List<Subtask> getSubtasks();

    @RequestMapping(method = RequestMethod.POST, value = "")
    Subtask insertSubtask(@RequestBody Subtask subtask);

    @RequestMapping(method = RequestMethod.PUT, value = "/{subtask_id}")
    Subtask updateSubtask(@RequestBody Subtask subtask, @PathVariable int subtask_id);
    
}
