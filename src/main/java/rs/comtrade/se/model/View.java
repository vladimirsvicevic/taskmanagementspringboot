/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

/**
 *
 * @author vsvicevic
 */
public class View {

    public interface UserView {
    }

    public interface TaskIncludeView extends UserView {
    }

    public interface SubtaskIncludeView extends TaskIncludeView {
    }

    public interface RolesIncludeView extends UserView {
    }

    public interface PermissionIncludeView extends RolesIncludeView {
    }

    public interface BaseEffortView extends UserView {
    }

    public interface TaskEffortView extends BaseEffortView, TaskIncludeView {
    }

    public interface SubtaskEffortView extends BaseEffortView, SubtaskIncludeView {
    }
}
