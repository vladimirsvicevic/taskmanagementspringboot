/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.model;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author vsvicevic
 */
public interface Tasks {

    @RequestMapping(method = RequestMethod.POST, value = "/{task_id}/user/{user_id}")
    void assignUser(@PathVariable int task_id, @PathVariable int user_id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/{task_id}")
    void deleteTask(@PathVariable int task_id);

    @RequestMapping(method = RequestMethod.GET, value = "")
    List<Task> getTasks();

    @RequestMapping(method = RequestMethod.POST, value = "")
    Task insertTask(@RequestBody Task task);

    @RequestMapping(method = RequestMethod.PUT, value = "/{task_id}")
    Task updateTask(@RequestBody Task task, @PathVariable int task_id);
    
}
