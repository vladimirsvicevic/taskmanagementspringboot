/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.comtrade.se.model.Effort;

/**
 *
 * @author vsvicevic
 */
@Repository
public interface EffortRepository extends CrudRepository<Effort, Integer>{
    
}
