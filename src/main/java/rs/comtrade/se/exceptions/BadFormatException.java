/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.exceptions;

/**
 *
 * @author vsvicevic
 */
public class BadFormatException extends Exception {

    public BadFormatException(String message) {
        super(message);
    }
}
