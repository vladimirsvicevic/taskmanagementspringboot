/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.controller;

import rs.comtrade.se.model.Users;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rs.comtrade.se.model.Effort;
import rs.comtrade.se.model.User;
import rs.comtrade.se.model.View;
import rs.comtrade.se.services.UserService;

/**
 *
 * @author vsvicevic
 */
@RestController
@RequestMapping(value = "api/users", consumes = "application/json", produces = "application/json")
public class UserController implements Users {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "")
    @Override
    public MappingJacksonValue getUsers(@RequestParam(value = "include", defaultValue = "notInclude") String include) {
        MappingJacksonValue wrapper = new MappingJacksonValue(userService.getUsers());
        switch (include) {
            case "tasks":
                wrapper.setSerializationView(View.TaskIncludeView.class);
                break;
            case "subtasks":
                wrapper.setSerializationView(View.SubtaskIncludeView.class);
                break;
            case "roles":
                wrapper.setSerializationView(View.RolesIncludeView.class);
                break;
            case "permissions":
                wrapper.setSerializationView(View.PermissionIncludeView.class);
                break;
            case "taskeffort":
                wrapper.setSerializationView(View.TaskEffortView.class);
                break;
            case "subtaskeffort":
                wrapper.setSerializationView(View.SubtaskEffortView.class);
                break;
            default:
                wrapper.setSerializationView(View.UserView.class);
        }
        return wrapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{user_id}")
    private MappingJacksonValue getUser(@PathVariable int user_id, @RequestParam(value = "include", defaultValue = "notInclude") String include) {
        MappingJacksonValue wrapper = new MappingJacksonValue(userService.getUser(user_id));
        switch (include) {
            case "tasks":
                wrapper.setSerializationView(View.TaskIncludeView.class);
                break;
            case "subtasks":
                wrapper.setSerializationView(View.SubtaskIncludeView.class);
                break;
            case "roles":
                wrapper.setSerializationView(View.RolesIncludeView.class);
                break;
            case "permissions":
                wrapper.setSerializationView(View.PermissionIncludeView.class);
                break;
            case "taskeffort":
                wrapper.setSerializationView(View.TaskEffortView.class);
                break;
            case "subtaskeffort":
                wrapper.setSerializationView(View.SubtaskEffortView.class);
                break;
            default:
                wrapper.setSerializationView(View.UserView.class);
        }
        return wrapper;
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    @Override
    public User insertUser(@RequestBody User user) {
        return userService.insertUser(user);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{user_id}")
    @Override
    public User updateUser(@RequestBody User user, @PathVariable int user_id) {
        return userService.updateUser(user, user_id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{user_id}")
    @Override
    public void deleteUser(@PathVariable int user_id) {
        userService.deleteUser(user_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/role/{role_id}")
    @Override
    public User assignRole(@PathVariable int user_id, @PathVariable int role_id) {
        return (User) userService.assignRole(user_id, role_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/task/{task_id}")
    @Override
    public User assignTask(@PathVariable int user_id, @PathVariable int task_id) {
        return (User) userService.assignTask(user_id, task_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/role/{role_id}/permission/{permission_id}")
    @Override
    public User assignPermission(@PathVariable int user_id, @PathVariable int role_id, @PathVariable int permission_id) {
        return (User) userService.assignPermission(user_id, role_id, permission_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/task/{task_id}/subtask/{subtask_id}")
    @Override
    public Response assignSubtask(@PathVariable int user_id, @PathVariable int task_id, @PathVariable int subtask_id) {
        return (Response) userService.assignSubtask(user_id, task_id, subtask_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/task/{task_id}/effort")
    @Override
    public Response assignEffortToTask(@PathVariable int user_id, @PathVariable int task_id, @RequestBody Effort effort) {
        return (Response) userService.assignEffortToTask(user_id, task_id, effort);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{user_id}/subtask/{subtask_id}/effort")
    @Override
    public Response assignEffortToSubtask(@PathVariable int user_id, @PathVariable int subtask_id, @RequestBody Effort effort) {
        return (Response) userService.assignEffortToSubtask(user_id, subtask_id, effort);
    }
}
