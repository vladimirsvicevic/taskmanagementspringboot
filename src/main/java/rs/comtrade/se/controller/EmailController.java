/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.controller;


import rs.comtrade.se.model.Emails;
import javax.ws.rs.core.Response;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.comtrade.se.exceptions.BadFormatException;
import rs.comtrade.se.model.Email;
import rs.comtrade.se.model.ErrorMessage;
import rs.comtrade.se.util.EmailUtil;

/**
 *
 * @author vsvicevic
 */
@RestController
public class EmailController implements Emails {
    
    @RequestMapping(method = RequestMethod.POST, value = "/email/validator")
    @Override
    public Response validateEmailAddress(@RequestBody Email email) {
        try {
            EmailUtil.validateEmail(email);
            return Response.status(Response.Status.OK)
                    .build();
        } catch (BadFormatException ex) {
            ErrorMessage emailErrorMessage = new ErrorMessage(ex.getMessage());
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(emailErrorMessage)
                    .build();
        }
    }
}
