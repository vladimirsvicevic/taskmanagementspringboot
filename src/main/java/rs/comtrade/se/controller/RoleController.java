/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.controller;

import rs.comtrade.se.model.Roles;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.comtrade.se.model.Role;
import rs.comtrade.se.services.RoleService;

/**
 *
 * @author vsvicevic
 */
@RestController
@RequestMapping("api/roles")
public class RoleController implements Roles {
    
    @Autowired
    private RoleService roleService;

    @RequestMapping(method = RequestMethod.GET, value = "")
    @Override
    public List<Role> getRoles() {
        return roleService.getRoles();
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{role_id}")
    private Role getRole(@PathVariable int role_id) {
        return roleService.getRole(role_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    @Override
    public Role insertRole(@RequestBody Role role) {
        return roleService.insertRole(role);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{role_id}")
    @Override
    public Role updateRole(@RequestBody Role role, @PathVariable int role_id) {
        return roleService.updateRole(role, role_id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value = "/{role_id}")
    @Override
    public void deleteRole(@PathVariable int role_id) {
        roleService.deleteRole(role_id);
    }
}
