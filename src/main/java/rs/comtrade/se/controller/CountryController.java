/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.controller;

import rs.comtrade.se.model.Countries;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.comtrade.se.model.Country;
import rs.comtrade.se.services.CountryService;

/**
 *
 * @author vsvicevic
 */
@RestController
@RequestMapping(value = "api/countries", consumes = "application/json", produces = "application/json")
public class CountryController implements Countries {

    @Autowired
    private CountryService countryService;

    @RequestMapping(method = RequestMethod.GET, value = "")
    @Override
    public List<Country> getCountries() {
        return countryService.getCountries();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{country_id}")
    private Country getCountry(@PathVariable int country_id) {
        return countryService.getCountry(country_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    @Override
    public Country insertCountry(@RequestBody Country country) {
        return countryService.insertCountry(country);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{country_id}")
    @Override
    public Country updateCountry(@RequestBody Country country, @PathVariable int country_id) {
        return countryService.updateCountry(country, country_id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value = "/{country_id}")
    @Override
    public void deleteCountry(@PathVariable int country_id) {
        countryService.deleteCountry(country_id);
    }
}
