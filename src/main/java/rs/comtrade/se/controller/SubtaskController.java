/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.controller;

import rs.comtrade.se.model.Subtasks;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.comtrade.se.model.Subtask;
import rs.comtrade.se.services.SubtaskService;

/**
 *
 * @author vsvicevic
 */
@RestController
@RequestMapping("api/subtasks")
public class SubtaskController implements Subtasks {

    @Autowired
    private SubtaskService subtaskService;
    
    @RequestMapping(method = RequestMethod.GET, value = "")
    @Override
    public List<Subtask> getSubtasks() {
        return subtaskService.getSubtasks();
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{subtask_id}")
    @Override
    public Subtask getSubtask(@PathVariable int subtask_id) {
        return subtaskService.getSubtask(subtask_id);
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "")
    @Override
    public Subtask insertSubtask(@RequestBody Subtask subtask) {
        return subtaskService.insertSubtask(subtask);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{subtask_id}")
    @Override
    public Subtask updateSubtask(@RequestBody Subtask subtask, @PathVariable int subtask_id) {
        return subtaskService.updateSubtask(subtask, subtask_id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value = "/{subtask_id}")
    @Override
    public void deleteSubtask(@PathVariable int subtask_id) {
        subtaskService.deleteSubtask(subtask_id);
    }
}
