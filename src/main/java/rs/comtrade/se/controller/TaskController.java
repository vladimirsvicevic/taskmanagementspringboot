/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.controller;

import rs.comtrade.se.model.Tasks;
import java.util.List;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.comtrade.se.model.Task;
import rs.comtrade.se.services.TaskService;

/**
 *
 * @author vsvicevic
 */
@RestController
@RequestMapping(value = "api/tasks", consumes = "application/json", produces = "application/json")
public class TaskController implements Tasks {

    @Autowired
    private TaskService taskService;

    @RequestMapping(method = RequestMethod.GET, value = "")
    @Override
    public List<Task> getTasks() {
        return taskService.getTasks();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{task_id}")
    private Task getTask(@PathVariable int task_id) {
        return taskService.getTask(task_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    @Override
    public Task insertTask(@RequestBody Task task) {
        return taskService.insertTask(task);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{task_id}")
    @Override
    public Task updateTask(@RequestBody Task task, @PathVariable int task_id) {
        return taskService.updateTask(task, task_id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{task_id}")
    @Override
    public void deleteTask(@PathVariable int task_id) {
        taskService.deleteTask(task_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{task_id}/user/{user_id}")
    @Override
    public void assignUser(@PathVariable int task_id, @PathVariable int user_id) {
        taskService.assignUser(task_id, user_id);
    }
}
