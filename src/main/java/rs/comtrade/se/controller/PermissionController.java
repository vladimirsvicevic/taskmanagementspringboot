/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.comtrade.se.controller;

import rs.comtrade.se.model.Permissions;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.comtrade.se.model.Permission;
import rs.comtrade.se.services.PermissionService;

/**
 *
 * @author vsvicevic
 */
@RestController
@RequestMapping("/api/permissions")
public class PermissionController implements Permissions {
    
    @Autowired
    private PermissionService permissionService;
    
    @RequestMapping(method = RequestMethod.GET, value = "")
    @Override
    public List<Permission> getPermissions() {
        return permissionService.getPermissions();
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{permission_id}")
    private Permission getPermission(@PathVariable int permission_id) {
        return permissionService.getPermission(permission_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "")
    @Override
    public Permission insertPermission(@RequestBody Permission permission) {
        return permissionService.insertPermission(permission);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{permission_id}")
    @Override
    public Permission updatePermission(@RequestBody Permission permission, @PathVariable int permission_id) {
        return permissionService.updatePermission(permission, permission_id);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value = "/{permission_id}")
    @Override
    public void deletePermission(@PathVariable int permission_id) {
        permissionService.deletePermission(permission_id);
    }
}
